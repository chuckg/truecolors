## Interface: 20100
## Title: |cffFF7D0At|r|cffABD473r|r|cff69CCF0u|r|cffF58CBAe|r|cffFFFFFFC|r|cffFFF569o|r|cff00DBBAl|r|cff9482CAo|r|cffC79C6Er|r|cffFF7D0As|r
## Notes: Changes the default raid color of shaman back to their wonderful |cff00DBBAturqoise|r, the 'new blue' be damned. 
## Version: 1.0
## Author: chuckg (Blindchuck <Ropetown>)
## X-eMail: c@chuckg.org
## X-Website: http://chuckg.wowinterface.com
## X-Category: Interface Enhancements
## X-Date: 2007-05-27
## OptionalDeps: 
## Dependencies: 

!trueColors.lua